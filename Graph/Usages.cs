﻿using Graph.Interfaces;
using Graph.PathFinding;
using NUnit.Framework;
using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

namespace Graph
{
    [TestFixture]
    internal class Usages
    {
        public struct Person
        {
            private readonly string _name;
            private readonly int _age;
            private readonly string _nationality;

            public Person(string name, int age, string nationality) => (_name, _age, _nationality) = (name, age, nationality);
            public override string ToString() => $"{_name}, age:{_age} ({_nationality})";
            public override bool Equals(object? obj) => obj is Person p && p.ToString() == ToString();
            public override int GetHashCode() => HashCode.Combine(_name, _age, _nationality);
        }

        [Test]
        public async Task SimpleGraph()
        {
            Graph<Person> friendshipGraph = new();

            // graph is populated directly...
            Node<Person> lucyNode = friendshipGraph.Add(new Person("Lucy", 25, "IT"));
            Node<Person> markNode = friendshipGraph.Add(new Person("Mark", 30, "ES"));

            // ...or through nodes creation
            Node<Person> maryNode = new Node<Person>(friendshipGraph, new Person("Mary", 35, "US"));
            Node<Person> johnNode = new Node<Person>(friendshipGraph, new Person("John", 51, "UK"));

            // edges are created using the nodes
            lucyNode.AddBidirectionalConnection(markNode);
            markNode.AddBidirectionalConnection(maryNode);
            maryNode.AddBidirectionalConnection(johnNode);

            // paths can be found easily
            Path<Person> lucyToJohn = await friendshipGraph.FirstDepthSearch(lucyNode, johnNode);
            string pathToString = lucyToJohn.ToString(person => person.ToString());
            Console.WriteLine(pathToString);

            // output: "Lucy, age:25 (IT) -> Mark, age:30 (ES) -> Mary, age:35 (US) -> John, age:51 (UK)"
        }

        [Test]
        public async Task MultiGraph()
        {
            Graph<Person> friendshipGraph = new();
            Node<Person> lucyNode = friendshipGraph.Add(new Person("Lucy", 25, "IT"));
            Node<Person> markNode = friendshipGraph.Add(new Person("Mark", 30, "ES"));
            Node<Person> maryNode = friendshipGraph.Add(new Person("Mary", 35, "US"));
            Node<Person> johnNode = friendshipGraph.Add(new Person("John", 51, "UK"));
            lucyNode.AddBidirectionalConnection(markNode);
            markNode.AddBidirectionalConnection(maryNode);
            maryNode.AddBidirectionalConnection(johnNode);

            Graph<Person> careerGraph = new Graph<Person>();
            Node<Person> lucyNodeCareer = careerGraph.Add(new Person("Lucy", 25, "IT"));
            Node<Person> johnNodeCareer = careerGraph.Add(new Person("John", 51, "UK"));
            lucyNodeCareer.AddConnection(johnNodeCareer);

            // paths can be found in multi-graphs
            ImmutableList<IGraph<Person>> multiGraph = new[] { friendshipGraph, careerGraph }.Cast<IGraph<Person>>().ToImmutableList();
            Path<Person> lucyToJohn = await multiGraph.AStarSearch(lucyNode.Value, johnNode.Value, person => 1d);
            string pathToString = lucyToJohn.ToString(person => person.ToString());
            Console.WriteLine(pathToString);

            // output "Lucy, age:25 (IT) -> John, age:51 (UK)"
        }
    }
}
