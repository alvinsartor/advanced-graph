﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Graph.Extensions;
using Graph.Interfaces;

namespace Graph
{
    /// <summary>
    /// A data structure representing a graph.
    /// </summary>
    /// <seealso cref="IGraph{T}" />
    public class Graph<T> : IGraph<T> where T : notnull
    {
        private readonly Dictionary<T, Node<T>> _nodes;
        private readonly Dictionary<Guid, Node<T>> _nodesIds;

        /// <summary>
        /// Initializes a new instance of the <see cref="Graph{T}"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public Graph(string? name = null)
        {
            Id = Guid.NewGuid();
            Name = name ?? $"graph {Id.ToString()}";
            _nodes = new Dictionary<T, Node<T>>();
            _nodesIds = new Dictionary<Guid, Node<T>>();

            OnNodeAdded = null;
            OnNodeRemoved = null;
        }

        /// <inheritdoc />
        public string Name { get; }

        /// <inheritdoc />
        public Guid Id { get; }

        /// <inheritdoc />
        public int Count => _nodes.Count;

        /// <summary>
        /// Gets the nodes of this graph.
        /// <remarks>Note: This call has complexity O(#Nodes).</remarks>
        /// </summary>
        public ImmutableHashSet<Node<T>> Nodes => _nodes.Values.ToImmutableHashSet();

        /// <inheritdoc />
        public ImmutableHashSet<T> Values => _nodes.Keys.ToImmutableHashSet();

        /// <inheritdoc />
        public Node<T> this[T value] =>
            Contains(value) ? _nodes[value] : throw new ArgumentException("Value not in graph.");

        /// <inheritdoc />
        public bool Contains(T value) => _nodes.ContainsKey(value.NotNull(nameof(value)));

        /// <inheritdoc />
        public bool Contains(Guid valueId) => _nodesIds.ContainsKey(valueId);

        /// <inheritdoc />
        public bool HasCycles() => _nodes.Values.Any(x => x.CyclesFound(ImmutableHashSet<Guid>.Empty));

        /// <inheritdoc />
        public event EventHandler<T>? OnNodeAdded;

        /// <inheritdoc />
        public event EventHandler<T>? OnNodeRemoved;

        /// <inheritdoc />
        public bool Equals(IGraph<T>? other)
        {
            return other != null && other.Id == Id;
        }

        /// <summary>
        /// Adds the specified item to the graph.
        /// </summary>   
        /// <exception cref="ArgumentException">A node with the same value already exists in the graph.</exception>    
        public virtual Node<T> Add(T item)
        {
            item.AssertNotNull(nameof(item));

            if (Contains(item))
            {
                throw new ArgumentException("A node with the same value already exists in the graph.");
            }

            var node = new Node<T>(this, item);
            _nodes.Add(item, node);
            _nodesIds.Add(node.Id, node);

            OnNodeAdded?.Invoke(this, item);

            return node;
        }

        /// <summary>
        /// Tries to add the specified item to the graph.
        /// </summary>   
        /// <returns>The added node, if it has been possible to add it,
        /// otherwise the node that was already in the graph.</returns>
        public virtual Node<T> TryAdd(T item)
        {
            item.AssertNotNull(nameof(item));
            return Contains(item) ? _nodes[item] : Add(item);
        }

        /// <summary>
        /// Removes the specified item from the graph.
        /// </summary>
        /// <exception cref="ArgumentException">Item not in graph</exception>
        public virtual void Remove(T item)
        {
            item.AssertNotNull(nameof(item));

            if (!Contains(item))
            {
                throw new ArgumentException("Item not in graph");
            }

            _nodesIds.Remove(_nodes[item].Id);
            _nodes.Remove(item);

            OnNodeRemoved?.Invoke(this, item);
        }

        /// <summary>
        /// Gets the node with the specified value.
        /// </summary>
        /// <param name="value">The value of the searched node.</param>
        /// <exception cref="ArgumentException">Value not in graph.</exception>
        public Node<T> GetNode(T value)
        {
            value.AssertNotNull(nameof(value));
            return Contains(value) ? _nodes[value] : throw new ArgumentException("Value not in graph.");
        }

        /// <summary>
        /// Tries the get the node with the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="node">The node.</param>
        public bool TryGetNode(T value, [NotNullWhen(true)] out Node<T>? node)
        {
            value.AssertNotNull(nameof(value));
            return _nodes.TryGetValue(value, out node);
        }

        /// <inheritdoc />
        public override bool Equals(object? other) => other is IGraph<T> graph && graph.Id == Id;

        /// <inheritdoc />
        public override int GetHashCode() => Id.GetHashCode();

        /// <inheritdoc />
        public new string ToString() => 
            _nodes.Values.Aggregate("", (current, node) => current + node.ToString() + Environment.NewLine);

        /// <inheritdoc />
        public string ToString(Func<T, string> toString)
        {
            toString.AssertNotNull(nameof(toString));
            return _nodes.Values.Aggregate("", (current, node) => 
                current + node.ToStringWithEdges(toString) + Environment.NewLine);
        }

    }
}