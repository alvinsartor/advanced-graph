﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Graph.Extensions
{
    /// <summary>
    /// Extensions on the LinkedList class.
    /// </summary>
    public static class LinkedListExtensions
    {
        /// <summary>
        /// Pushes a collection of elements on the list, adding at the beginning.
        /// </summary>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="elements">The elements.</param>
        public static void PushFirst<T>(this LinkedList<T> list, IEnumerable<T> elements)
        {
            list.AssertNotNull(nameof(list));
            elements.AssertNotNull(nameof(elements));

            foreach (var element in elements.Reverse())
            {
                list.AddFirst(element);
            }
        }

        /// <summary>
        /// Pushes a collection of elements on the list, adding at the end.
        /// </summary>
        /// <typeparam name="T">The type of elements in the list.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="elements">The elements.</param>
        public static void PushLast<T>(this LinkedList<T> list, IEnumerable<T> elements)
        {
            list.AssertNotNull(nameof(list));
            elements.AssertNotNull(nameof(elements));

            foreach (var element in elements)
            {
                list.AddLast(element);
            }
        }

        /// <summary>
        /// Adds the element to the ordered list.
        /// </summary>
        /// <typeparam name="T">The element type.</typeparam>
        /// <param name="list">The list.</param>s
        /// <param name="element">The element to add.</param>
        /// <param name="comparer">The comparer.</param>
        public static void AddOrdered<T>(this LinkedList<T> list, T element, Func<T, double> comparer)
        {
            list.AssertNotNull(nameof(list));
            element.AssertNotNull(nameof(element));
            comparer.AssertNotNull(nameof(comparer));

            var tValue = comparer(element);
            if (list.Count == 0 || comparer(list.First!.Value) > tValue)
            {
                list.AddFirst(element);
            }
            else
            {
                LinkedListNode<T>? temp = list.First;
                while (temp != null)
                {
                    if (temp.Next == null || comparer(temp.Next.Value) > tValue)
                    {
                        list.AddAfter(temp, element);
                        temp = null;
                    }
                    else
                    {
                        temp = temp.Next;
                    }
                }
            }
        }

        /// <summary>
        /// Pops the first element of the list.
        /// </summary>
        /// <typeparam name="T">Type of the elements in the list.</typeparam>
        /// <param name="list">The list.</param>
        /// <returns>The first element of the list.</returns>
        public static T PopFirst<T>(this LinkedList<T> list)
        {
            list.AssertNotNullOrEmpty(nameof(list));

            var element = list.First!;
            list.RemoveFirst();
            return element.Value;
        }

        /// <summary>
        /// Pops the last element of the list.
        /// </summary>
        /// <typeparam name="T">Type of the elements in the list.</typeparam>
        /// <param name="list">The list.</param>
        /// <returns>The last element of the list.</returns>
        public static T PopLast<T>(this LinkedList<T> list)
        {
            list.AssertNotNullOrEmpty(nameof(list));

            var element = list.Last!;
            list.RemoveLast();
            return element.Value;
        }

    }
}