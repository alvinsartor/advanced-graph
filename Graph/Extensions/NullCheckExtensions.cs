﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Graph.Extensions
{
    public static class NullCheckExtensions
    {
        /// <summary>
        /// Asserts that the instance is not null.
        /// </summary>
        /// <param name="instance">The instance to check.</param>
        /// <param name="variableName">Name of the variable, for the message.</param>
        /// <exception cref="ArgumentException">The element is null.</exception>
        [Pure]
        public static T NotNull<T>(this T instance, string variableName)
        {
            return instance != null ? instance : throw new ArgumentException($"{variableName} is null.");
        }

        /// <summary>
        /// Asserts that the instance is not null.
        /// </summary>
        /// <param name="instance">The instance to check.</param>
        /// <param name="variableName">Name of the variable, for the message.</param>
        /// <exception cref="ArgumentException">The element is null.</exception>
        public static void AssertNotNull<T>([NoEnumeration] this T instance, string variableName)
        {
            if (instance == null)
            {
                throw new ArgumentException($"{variableName} is null.");
            }
        }

        /// <summary>
        /// Asserts that the collection is not null and not empty.
        /// </summary>
        /// <param name="instance">The instance to check.</param>
        /// <param name="variableName">Name of the variable, for the message.</param>
        /// <exception cref="ArgumentException">The collection is null.</exception>
        /// <exception cref="ArgumentException">The collection is empty.</exception>
        public static void AssertNotNullOrEmpty<T>(this ICollection<T> instance, string variableName)
        {
            if (instance == null)
            {
                throw new ArgumentException($"{variableName} is null.");
            }
            
            if (instance.Count == 0)
            {
                throw new ArgumentException($"{variableName} is empty.");
            }
        }
    }
}