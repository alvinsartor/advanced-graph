﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Graph.Extensions.NUnit
{
    [TestFixture]
    internal sealed class NullCheckFixture
    {
        [Test]
        public void AssertNotNullThrowsWhenNull()
        {
            object obj = null!;

            var exception = Assert.Throws<ArgumentException>(() => obj.AssertNotNull("obj"));
            Assert.That(exception.Message, Is.EqualTo("obj is null."));

            obj = new object();
            Assert.DoesNotThrow(() => obj.AssertNotNull("obj"));            
        }

        [Test]
        public void AssertNotNullOrEmptyThrowsWhenNullOrEmpty()
        {
            List<int> list = null!;

            var exception = Assert.Throws<ArgumentException>(() => list.AssertNotNullOrEmpty("list"));
            Assert.That(exception.Message, Is.EqualTo("list is null."));

            list = new List<int>();
            exception = Assert.Throws<ArgumentException>(() => list.AssertNotNullOrEmpty("list"));
            Assert.That(exception.Message, Is.EqualTo("list is empty."));
            
            list.Add(1);
            Assert.DoesNotThrow(() => list.AssertNotNull("list"));
        }

        [Test]
        public void NotNullThrowsWhenNull()
        {
            object obj = null!;
            object variable = null!;

            var exception = Assert.Throws<ArgumentException>(() => variable = obj.NotNull("obj"));
            Assert.That(exception.Message, Is.EqualTo("obj is null."));
            Assert.Null(variable);

            obj = new object();
            Assert.DoesNotThrow(() => variable = obj.NotNull("obj"));
            Assert.NotNull(variable);
        }
    }
}
