﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Graph.Extensions.NUnit
{
    [TestFixture]
    internal sealed class EnumerableExtensionsFixture
    {
        private Random? _random;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _random = new Random();
        }

        private struct Person
        {
            public int Age;
            public static Person RandomPerson(Random r) => new Person {Age = r.Next(0, 99)};
        }

        [Test, Repeat(10)]
        public void MaxByTest()
        {
            var people = Enumerable.Repeat(Person.RandomPerson(_random!), 20).ToList();
            var oldest = people.MaxBy(person => person.Age);

            Assert.That(oldest.Age, Is.GreaterThanOrEqualTo(people.Max(x => x.Age)));
        }

        [Test]
        public void MaxByThrowsIfCollectionIsNullOrEmpty()
        {
            List<Person> people = null!;
            Assert.Throws<ArgumentNullException>(() => _ = people.MaxBy(person => person.Age));

            people = new List<Person>();
            Assert.Throws<ArgumentException>(() => _ = people.MaxBy(person => person.Age));
        }

        [Test, Repeat(10)]
        public void MinByTest()
        {
            var people = Enumerable.Repeat(Person.RandomPerson(_random!), 20).ToList();
            var youngest = people.MinBy(person => person.Age);

            Assert.That(youngest.Age, Is.LessThanOrEqualTo(people.Max(x => x.Age)));
        }

        [Test]
        public void MinByThrowsIfCollectionIsNullOrEmpty()
        {
            List<Person> people = null!;
            Assert.Throws<ArgumentNullException>(() => _ = people.MinBy(person => person.Age));

            people = new List<Person>();
            Assert.Throws<ArgumentException>(() => _ = people.MinBy(person => person.Age));
        }
    }
}