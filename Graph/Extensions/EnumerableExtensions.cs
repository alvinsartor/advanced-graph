﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Graph.Extensions
{
    /// <summary>
    /// Extensions of the IEnumerable class.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Returns the maximum element of a collection using a function to calculate its value
        /// </summary>
        /// <typeparam name="T">The type of object in the list.</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <param name="valueExtractor">The function to get the value out of T.</param>
        [Pure]
        public static T MaxBy<T>(this IEnumerable<T> enumerable, Func<T, double> valueExtractor)
        {
            var collection = enumerable.ToList();
            if (collection.Count == 0) throw new ArgumentException("Cannot extract MaxBy from an empty collection.");

            T currentMax = collection.First();
            var currentMaxValue = valueExtractor(currentMax);

            foreach (var number in collection)
            {
                var tmpValue = valueExtractor(number);
                if (tmpValue > currentMaxValue)
                {
                    currentMax = number;
                    currentMaxValue = tmpValue;
                }
            }

            return currentMax;
        }

        /// <summary>
        /// Returns the minimum element of a collection using a function to calculate its value
        /// </summary>
        /// <typeparam name="T">The type of object in the collection.</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <param name="valueExtractor">The function to get the value out of T.</param>
        [Pure]
        public static T MinBy<T>(this IEnumerable<T> enumerable, Func<T, double> valueExtractor)
        {
            var collection = enumerable.ToList();
            if (collection.Count == 0) throw new ArgumentException("Cannot extract MinBy from an empty collection.");

            var currentMin = collection.First();
            var currentMinValue = valueExtractor(currentMin);

            foreach (var number in collection)
            {
                var tmpValue = valueExtractor(number);
                if (tmpValue < currentMinValue)
                {
                    currentMin = number;
                    currentMinValue = tmpValue;
                }
            }

            return currentMin;
        }
    }
}