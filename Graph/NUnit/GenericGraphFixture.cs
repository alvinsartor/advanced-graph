using System;
using System.Linq;
using NUnit.Framework;

namespace Graph.NUnit
{
    [TestFixture]
    internal abstract class GenericGraphFixture<TGraph>
        where TGraph : Graph<int>
    {
        protected abstract TGraph CreateGraph();

        [Test]
        public void GraphIsInitializedEmpty()
        {
            var graph = CreateGraph();

            Assert.That(graph.Count, Is.EqualTo(0));
            Assert.That(graph.Nodes.Count, Is.EqualTo(0));
            Assert.That(graph.Values.Count, Is.EqualTo(0));
        }

        [Test]
        public void TheCountOfElementsIncreasesAfterAdditionAndDecreasesAfterDeletion()
        {
            var graph = CreateGraph();
            const int value = 42;

            Assert.That(graph.Count, Is.EqualTo(0));

            graph.Add(value);
            Assert.That(graph.Count, Is.EqualTo(1));
            Assert.That(graph.Contains(value));

            graph.Remove(value);
            Assert.That(graph.Count, Is.EqualTo(0));
        }

        [Test]
        public void GraphThrowsIfTryingToAddAnAlreadyExistingValue()
        {
            var graph = CreateGraph();
            const int value = 42;

            graph.Add(value);
            Assert.That(graph.Contains(value));

            Assert.Throws<ArgumentException>(() => graph.Add(value));
        }

        [Test]
        public void GraphThrowsIfTryingToDeleteAValueNotInTheCollection()
        {
            var graph = CreateGraph();
            const int value = 42;

            Assert.That(!graph.Contains(value));
            Assert.Throws<ArgumentException>(() => graph.Remove(value));
        }

        [Test]
        public void GraphThrowsIfTryingToGetAValueNotInTheCollection()
        {
            var graph = CreateGraph();
            const int value = 42;

            Assert.That(!graph.Contains(value));
            Assert.Throws<ArgumentException>(() => graph.GetNode(value));
            Assert.Throws<ArgumentException>(() =>
            {
                var _ = graph[value];
            });

            Assert.DoesNotThrow(() => graph.TryGetNode(value, out _));
        }

        [Test]
        public void GraphContainsAreFindingExistingNodes()
        {
            var graph = CreateGraph();
            const int value = 42;

            Assert.That(!graph.Contains(value));
            Assert.That(!graph.Contains(new Guid()));

            graph.Add(value);

            Assert.That(graph.Contains(value));
            Assert.That(graph.Contains(graph[value].Id));
        }

        [Test]
        public void CyclomaticConnectionsAreSpotted()
        {
            var graph = CreateGraph();
            const int alpha = 38;
            const int beta = 39;
            const int gamma = 40;
            const int delta = 41;
            const int omega = 42;

            var nodeAlpha = graph.Add(alpha);
            var nodeBeta = graph.Add(beta);
            var nodeGamma = graph.Add(gamma);
            var nodeDelta = graph.Add(delta);
            var nodeOmega = graph.Add(omega);

            Assert.False(graph.HasCycles());

            nodeAlpha.AddConnection(nodeBeta);
            nodeBeta.AddConnection(nodeGamma);
            nodeDelta.AddConnection(nodeOmega);
            nodeGamma.AddConnection(nodeOmega);

            Assert.False(graph.HasCycles());

            nodeGamma.AddConnection(nodeAlpha);
            Assert.True(graph.HasCycles());

            nodeGamma.RemoveConnection(nodeAlpha);
            Assert.False(graph.HasCycles());
        }

        [Test]
        public void BidirectionalConnectionsImmediatelyCreateCycles()
        {
            var graph = CreateGraph();
            const int alpha = 38;
            const int beta = 39;

            var nodeAlpha = graph.Add(alpha);
            var nodeBeta = graph.Add(beta);

            Assert.False(graph.HasCycles());

            nodeAlpha.AddBidirectionalConnection(nodeBeta);
            Assert.True(graph.HasCycles());

            nodeBeta.RemoveConnection(nodeAlpha);
            Assert.False(graph.HasCycles());
        }

        [Test]
        public void EventIsRaisedWhenNodeIsCreated()
        {
            var graph = CreateGraph();

            var resultFromCallback = 0;
            void Callback(object? o, int node) => resultFromCallback = node;

            graph.OnNodeAdded += Callback;

            try
            {
                graph.Add(12345);
                Assert.That(resultFromCallback, Is.EqualTo(12345));

                graph.TryAdd(555);
                Assert.That(resultFromCallback, Is.EqualTo(555));

                graph.TryAdd(12345);
                Assert.That(resultFromCallback, Is.EqualTo(555));
            }
            finally
            {
                graph.OnNodeAdded -= Callback;                
            }
        }

        [Test]
        public void EventIsRaisedWhenNodeIsRemoved()
        {
            var graph = CreateGraph();

            var resultFromCallback = 0;
            void Callback(object? o, int node) => resultFromCallback = node;

            graph.OnNodeRemoved += Callback;

            try
            {
                graph.Add(1);
                graph.Add(2);
                graph.Add(3);
                graph.Add(4);

                graph.Remove(3);
                Assert.That(resultFromCallback, Is.EqualTo(3));

                try
                {
                    graph.Remove(5);
                }
                catch (Exception)
                {
                    // exception is expected
                }

                Assert.That(resultFromCallback, Is.EqualTo(3));                
            }
            finally
            {
                graph.OnNodeRemoved -= Callback;
            }
        }
    }
}