﻿using System;
using NUnit.Framework;

namespace Graph.NUnit
{
    [TestFixture]
    internal sealed class EdgeFixture
    {
        [Test]
        public void EdgeIsInitializedCorrectly()
        {
            var graph = new Graph<int>();
            var node42 = graph.Add(42);
            var node38 = graph.Add(38);

            var edge = new Edge<int>(node42, node38, 5.0);

            Assert.That(edge.StartNode.Value == 42);
            Assert.That(edge.DestinationNode.Value == 38);
            Assert.That(edge.Weight, Is.EqualTo(5));
        }

        [Test]
        public void StringIsObtainedWhenToStringIsCalled()
        {
            var graph = new Graph<int>();
            var node42 = graph.Add(42);
            var node38 = graph.Add(38);

            var edge = new Edge<int>(node42, node38, 5.0);

            var expectedString = $"{node42} -> {node38}, [w:5]";

            Assert.That(edge.ToString(), Is.EqualTo(expectedString));
            Console.WriteLine(edge.ToString());
        }
    }
}
