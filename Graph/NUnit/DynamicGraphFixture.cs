﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;
using Graph.PathFinding;
using NUnit.Framework;

namespace Graph.NUnit
{
    [TestFixture]
    internal sealed class DynamicGraphFixture : GenericGraphFixture<DynamicGraph<int>>
    {
        private const int NumberOfChildrenForEachNode = 2;

        
        private static Task<ImmutableList<(int, double)>> ExpandFunction(int nodeValue)
        {
            var firstChild = nodeValue * NumberOfChildrenForEachNode + 1;
            var children = new List<(int, double)>();
            for (var i = 0; i < NumberOfChildrenForEachNode; i++)
            {
                children.Add((firstChild + i, 1d));
            }

            return Task.FromResult(children.ToImmutableList());
        }

        /// <inheritdoc />
        protected override DynamicGraph<int> CreateGraph()
        {            
            return new DynamicGraph<int>(ExpandFunction);
        }

        [Test]
        public async Task PathFindingTest()
        {
            var graph = CreateGraph();
            graph.Add(0);

            var path0To10 = await graph.AStarSearch(0, 100, val => Math.Abs(100 - val));
            var result = path0To10.ToString(x => x.ToString());

            Assert.That(result, Is.EqualTo("0 -> 2 -> 5 -> 11 -> 24 -> 49 -> 100"));
        }
    }
}
