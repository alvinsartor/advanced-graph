using NUnit.Framework;

namespace Graph.NUnit
{
    [TestFixture]
    internal sealed class GraphFixture : GenericGraphFixture<Graph<int>>
    {
        /// <inheritdoc />
        protected override Graph<int> CreateGraph()
        {
            return new Graph<int>();
        }
    }
}