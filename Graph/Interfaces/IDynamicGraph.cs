﻿using System.Collections.Immutable;
using System.Threading.Tasks;

namespace Graph.Interfaces
{
    /// <summary>
    /// Graph that is dynamically created though an expand function.
    /// Especially useful when the search space is very big or infinite, as the Dynamic graph would expand only the necessary nodes.
    /// </summary>
    /// <seealso cref="IGraph{T}" />
    public interface IDynamicGraph<T> : IGraph<T> where T : notnull
    {
        /// <summary>
        /// Sets the node containing the specified value as unexplored.
        /// </summary>
        /// <param name="value">The value to set as unexplored.</param>
        void SetUnexplored(T value);

        /// <summary>
        /// Sets the specified node as unexplored.
        /// </summary>
        /// <param name="node">The node to set as unexplored.</param>
        void SetUnexplored(Node<T> node);

        /// <summary>
        /// Sets the unexplored flag on all nodes.
        /// </summary>
        void SetAllUnexplored();

        /// <summary>
        /// Expands the specified node and adds its children to the graph.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>A list containing the newly found nodes.</returns>
        Task<ImmutableList<Node<T>>> Expand(Node<T> node);

        /// <summary>
        /// Expands the node containing the specified value and adds its children to the graph.
        /// </summary>
        /// <param name="value">The value to expand. There must be a node containing that value.</param>
        /// <returns>A list containing the newly found nodes.</returns>
        Task<ImmutableList<Node<T>>> Expand(T value);
    }
}