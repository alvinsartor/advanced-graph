﻿using System;
using System.Diagnostics.Contracts;

namespace Graph
{
    internal sealed class NodeState
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NodeState"/> class.
        /// </summary>
        /// <param name="isUnexplored">if set to <c>true</c> the node is unexplored (needs to be explored).</param>
        /// <param name="lastUpdate">The time of the last update.</param>
        /// <param name="dateAdded">The addition time.</param>
        public NodeState(bool isUnexplored, DateTime lastUpdate, DateTime dateAdded)
        {
            IsUnexplored = isUnexplored;
            LastUpdate = lastUpdate;
            DateAdded = dateAdded;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is unexplored.
        /// </summary>
        public bool IsUnexplored { get; }

        /// <summary>
        /// Gets the time of the last update.
        /// </summary>
        public DateTime LastUpdate { get; }

        /// <summary>
        /// Gets the time this node has been added.
        /// </summary>
        public DateTime DateAdded { get; }

        /// <summary>
        /// Returns a new state with the specified unexplored value.
        /// </summary>
        /// <param name="unexplored">if set to <c>true</c> the node is unexplored.</param>
        [Pure]
        public NodeState WithUpdate(bool unexplored) => new NodeState(unexplored, DateTime.UtcNow, DateAdded);
    }
}