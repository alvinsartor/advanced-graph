﻿using System;
using Graph.Extensions;

namespace Graph
{
    /// <summary>
    /// A weighted connection between two nodes.
    /// </summary>
    public sealed class Edge<T> : IEquatable<Edge<T>> where T : notnull
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Edge{T}"/> class.
        /// </summary>
        /// <param name="fromNode">Node the edge is starting from.</param>
        /// <param name="toNode">Node the edge is arriving to.</param>
        /// <param name="weight">The weight of the edge.</param>
        internal Edge(Node<T> fromNode, Node<T> toNode, double weight = 1)
        {
            Weight = weight;
            StartNode = fromNode.NotNull(nameof(fromNode));
            DestinationNode = toNode.NotNull(nameof(toNode));
        }

        /// <summary>
        /// The node the edge points to.
        /// </summary>
        public Node<T> DestinationNode { get; }

        /// <summary>
        /// The node the edge starts from. 
        /// </summary>
        public Node<T> StartNode { get; }

        /// <summary>
        /// The cost needed to cross the edge.
        /// </summary>
        public double Weight { get; }

        /// <inheritdoc />
        public bool Equals(Edge<T>? other) => 
            other != null
            && DestinationNode.Id.Equals(other.DestinationNode.Id)
            && StartNode.Id.Equals(other.StartNode.Id)
            && Weight.Equals(other.Weight);

        /// <inheritdoc />
        public override bool Equals(object? other) => other is Edge<T> edge && Equals(edge);

        /// <inheritdoc />
        public override int GetHashCode() => HashCode.Combine(DestinationNode, StartNode, Weight);

        /// <summary>
        /// Converts the edge to its string representation.
        /// </summary>
        public new string ToString() => $"{StartNode} -> {DestinationNode}, [w:{Weight}]";

        /// <summary>
        /// Converts the edge to its string representation.
        /// </summary>
        /// <param name="valueToString">Function used to transform the generic value to string.</param>
        public string ToString(Func<T, string> valueToString) => 
            $"{StartNode.ToString(valueToString)} -> {DestinationNode.ToString(valueToString)} [w:{Weight}]";
    }
}