using System;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Graph.Extensions;
using Graph.Interfaces;

namespace Graph
{
    /// <summary>
    /// Class that represents nodes in graphs.
    /// </summary>
    public sealed class Node<T> : IEquatable<Node<T>> where T : notnull
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Node{T}"/> class.
        /// </summary>
        internal Node(IGraph<T> graph, T value)
        {
            Id = Guid.NewGuid();
            Value = value.NotNull(nameof(value));
            Graph = graph.NotNull(nameof(graph));
            Edges = ImmutableDictionary<T, Edge<T>>.Empty;
            IncomingEdges = ImmutableDictionary<T, Edge<T>>.Empty;

            OnConnectionAdded = null;
            OnConnectionRemoved = null;
        }
        
        /// <summary>
        /// Event triggered whenever a connection from this node is added.
        /// </summary>
        public event EventHandler<T>? OnConnectionAdded;

        /// <summary>
        /// Event triggered whenever a connection from this node is removed.
        /// </summary>        
        public event EventHandler<T>? OnConnectionRemoved;

        /// <summary>
        /// Gets the node value.
        /// </summary>
        public T Value { get; }

        /// <summary>
        /// Gets the graph that holds this node.
        /// </summary>
        public IGraph<T> Graph { get; }

        /// <summary>
        /// Gets the edges of this node.
        /// </summary>
        public ImmutableDictionary<T, Edge<T>> Edges { get; private set; }

        /// <summary>
        /// Gets the edges pointing to this node.
        /// </summary>
        public ImmutableDictionary<T, Edge<T>> IncomingEdges { get; private set; }

        /// <summary>
        /// Gets the connections of this node.
        /// </summary>
        public ImmutableList<Node<T>> CurrentNeighbors =>
            Edges.Values.Select(x => x.DestinationNode).ToImmutableList();

        /// <summary>
        /// Gets all the connections of this node, expanding the node if it is possible.
        /// </summary>
        public async Task<ImmutableList<Node<T>>> GetAllNeighbors() => 
            Graph is DynamicGraph<T> dynamicGraph
                ? await dynamicGraph.Expand(this)
                : CurrentNeighbors;

        /// <summary>
        /// Gets the out-degree (the number of outgoing edges) of the node.
        /// </summary>
        public int OutDegree => Edges.Count;

        /// <summary>
        /// Gets the in-degree (the number of incoming edges) of the node.
        /// </summary>
        public int InDegree => IncomingEdges.Count;

        /// <summary>
        /// Gets the node identifier.
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Determines whether the current node is connected to the specified node.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>
        ///   <c>true</c> if there is an edge to the specified node; otherwise, <c>false</c>.
        /// </returns>
        public bool IsConnectedTo(Node<T> node)
        {
            node.AssertNotNull(nameof(node));
            return Edges.ContainsKey(node.Value);
        }

        /// <summary>
        /// Determines whether the current node is connected to a node with the specified value.
        /// </summary>
        /// <param name="value">The value to seek in the connections.</param>
        /// <returns>
        ///   <c>true</c> if there is an edge to the specified value; otherwise, <c>false</c>.s
        /// </returns>
        public bool IsConnectedTo(T value)
        {
            value.AssertNotNull(nameof(value));
            return Edges.ContainsKey(value);
        }

        /// <summary>
        /// Function used to spot cycles in the graph.
        /// </summary>
        /// <param name="encounteredNodes">The encountered nodes.</param>
        internal bool CyclesFound(ImmutableHashSet<Guid> encounteredNodes)
        {
            encounteredNodes.AssertNotNull(nameof(encounteredNodes));
            if (encounteredNodes.Contains(Id)) return true;

            encounteredNodes = encounteredNodes.Add(Id);
            return Edges.Any(x => x.Value.DestinationNode.CyclesFound(encounteredNodes));
        }

        /// <summary>
        /// Converts the node to its string representation.
        /// </summary>
        public new string ToString() => Value.ToString() ?? "null";

        /// <summary>
        /// Converts the node to its string representation.
        /// </summary>
        /// <param name="valueToString">Function used to transform the value of the node to string.</param>
        public string ToString(Func<T, string> valueToString) => valueToString(Value);

        /// <summary>
        /// Converts the node to its string representation and prints its edges.
        /// </summary>
        /// <param name="valueToString">Function used to transform the value of the node to string.</param>
        public string ToStringWithEdges(Func<T, string> valueToString)
        {
            return Edges.Values.Aggregate(
                ToString(valueToString) + Environment.NewLine,
                (current, edge) => current + "   " + edge.ToString(valueToString) + Environment.NewLine);
        }

        /// <summary>
        /// Adds a directional edge from this node to the specified one.
        /// </summary>
        /// <param name="node">The node to connect to.</param>
        /// <param name="weight">The weight of the edge.</param>
        /// <exception cref="ArgumentException">The selected node already has this edge.</exception>
        public void AddConnection(Node<T> node, double weight = 1)
        {
            node.AssertNotNull(nameof(node));

            if (IsConnectedTo(node)) throw new ArgumentException("The selected node already has this edge.");

            var edge = new Edge<T>(this, node, weight);
            Edges = Edges.Add(node.Value, edge);
            node.AddReverseConnection(edge);
            OnConnectionAdded?.Invoke(this, node.Value);
        }

        internal void AddReverseConnection(Edge<T> edge) => 
            IncomingEdges = IncomingEdges.Add(edge.StartNode.Value, edge);

        /// <summary>
        /// Adds a directional edge from this node to the specified one.
        /// </summary>
        /// <param name="node">The node to connect to.</param>
        /// <param name="weight">The weight of the edge.</param>
        public void TryAddConnection(Node<T> node, double weight = 1)
        {
            node.AssertNotNull(nameof(node));

            if (IsConnectedTo(node)) return;

            var edge = new Edge<T>(this, node, weight);
            Edges = Edges.Add(node.Value, edge);
            node.AddReverseConnection(edge);
            OnConnectionAdded?.Invoke(this, node.Value);
        }

        /// <summary>
        /// Adds a directional edge from this node to the specified one.
        /// If the edge already exists it is replaced with the new one.
        /// </summary>
        /// <param name="node">The node to connect to.</param>
        /// <param name="weight">The weight of the edge.</param>
        public void AddOrModifyConnection(Node<T> node, double weight = 1)
        {
            node.AssertNotNull(nameof(node));
            var edge = new Edge<T>(this, node, weight);

            if (IsConnectedTo(node))
            {
                Edges = Edges.Remove(node.Value);
                node.RemoveReverseConnection(Value);
                OnConnectionRemoved?.Invoke(this, node.Value);
            }

            Edges = Edges.Add(node.Value, edge);
            node.AddReverseConnection(edge);
            OnConnectionAdded?.Invoke(this, node.Value);
        }

        /// <summary>
        /// Adds a bidirectional edge between this node and the specified one.
        /// </summary>
        /// <param name="node">The node to connect to.</param>
        /// <param name="weight">The weight of the edge from this to node.</param>
        /// <param name="weightReturn">The weight of the edge from node to this.</param>
        public void AddBidirectionalConnection(Node<T> node, double weight = 1, double weightReturn = 1)
        {
            node.AssertNotNull(nameof(node));
            AddConnection(node, weight);
            node.AddConnection(this, weightReturn);
        }

        /// <summary>
        /// Adds a bidirectional edge between this node and the specified one if it does not exist already.
        /// </summary>
        /// <param name="node">The node to connect to.</param>
        /// <param name="weight">The weight of the edge from this to node.</param>
        /// <param name="weightReturn">The weight of the edge from node to this.</param>
        public void TryAddBidirectionalConnection(Node<T> node, double weight = 1, double weightReturn = 1)
        {
            node.AssertNotNull(nameof(node));
            TryAddConnection(node, weight);
            node.TryAddConnection(this, weightReturn);
        }

        /// <summary>
        /// Removes the edge from this node to the specified one.
        /// </summary>
        /// <param name="node">The node to remove the edge to.</param>
        /// <exception cref="ArgumentException">The edge with the specified node hasn't been found.</exception>
        public void RemoveConnection(Node<T> node)
        {
            node.AssertNotNull(nameof(node));

            if (!IsConnectedTo(node))
                throw new ArgumentException("The Edge with the specified node hasn't been found.");

            Edges = Edges.Remove(node.Value);
            node.RemoveReverseConnection(Value);
            OnConnectionRemoved?.Invoke(this, node.Value);
        }

        internal void RemoveReverseConnection(T value) =>
            IncomingEdges = IncomingEdges.Remove(value);

        /// <summary>
        /// Tries the remove the edge to the specified node.
        /// </summary>
        /// <param name="node">The node to remove the edge to.</param>
        public void TryRemoveConnection(Node<T> node)
        {
            node.AssertNotNull(nameof(node));

            if (!IsConnectedTo(node)) return;

            Edges = Edges.Remove(node.Value);
            node.RemoveReverseConnection(Value);
            OnConnectionRemoved?.Invoke(this, node.Value);
        }

        /// <summary>
        /// Removes all edges of this node.
        /// </summary>
        public void ClearConnections()
        {
            var connectedNodes = Edges.Values.Select(edge => edge.DestinationNode).ToList();
            foreach (var node in connectedNodes) RemoveConnection(node);
        }

        /// <inheritdoc />
        public override bool Equals(object? obj) => obj is Node<T> node && Equals(node);

        /// <inheritdoc />
        public bool Equals([AllowNull] Node<T> other) => other != null && other.Id == Id;

        /// <inheritdoc />
        public override int GetHashCode() => HashCode.Combine(Id, Graph);
    }
}