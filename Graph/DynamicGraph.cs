﻿using Graph.Extensions;
using Graph.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;

namespace Graph
{
    /// <summary>
    /// Graph that is dynamically created though an expand function.
    /// Especially useful when the search space is very big or infinite, as the Dynamic graph would expand only the necessary nodes.
    /// </summary>
    /// <seealso cref="Graph{T}" />
    /// <seealso cref="IDynamicGraph{T}" />
    public sealed class DynamicGraph<T> : Graph<T>, IDynamicGraph<T> where T : notnull
    {
        private readonly Func<T, Task<ImmutableList<(T, double)>>> _expandFunction;
        private readonly Dictionary<Node<T>, NodeState> _nodesState;

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicGraph{T}"/> class.
        /// </summary>
        /// <param name="expandFunction">The expand function.</param>
        public DynamicGraph(Func<T, Task<ImmutableList<(T, double)>>> expandFunction) : base()
        {
            _expandFunction = expandFunction;
            _nodesState = new Dictionary<Node<T>, NodeState>();
        }

        /// <inheritdoc />
        public override Node<T> Add(T item)
        {
            var addedNode = base.Add(item);
            _nodesState.Add(addedNode, new NodeState(true, DateTime.UtcNow, DateTime.UtcNow));
            return addedNode;
        }

        /// <inheritdoc />
        public override Node<T> TryAdd(T item)
        {
            return Contains(item) ? GetNode(item) : Add(item);
        }

        /// <inheritdoc />
        public override void Remove(T item)
        {
            item.AssertNotNull(nameof(item));

            if (!Contains(item))
            {
                throw new ArgumentException("Item not in graph");
            }

            _nodesState.Remove(this[item]);
            base.Remove(item);
        }

        /// <inheritdoc />
        public void SetUnexplored(Node<T> node)
        {
            node.AssertNotNull(nameof(node));
            SetUnexplored(node.Value);
        }

        /// <inheritdoc />
        public void SetAllUnexplored()
        {
            foreach (var node in _nodesState.Keys)
            {
                _nodesState[node] = _nodesState[node].WithUpdate(true);
            }
        }

        /// <inheritdoc />
        public void SetUnexplored(T value)
        {
            value.AssertNotNull(nameof(value));

            if (!Contains(value))
            {
                throw new ArgumentException($"Item '{value}' not in graph");
            }

            _nodesState[this[value]] = _nodesState[this[value]].WithUpdate(true);
        }

        /// <inheritdoc />
        public async Task<ImmutableList<Node<T>>> Expand(Node<T> node)
        {
            node.AssertNotNull(nameof(node));
            return await Expand(node.Value);
        }

        /// <inheritdoc />
        public async Task<ImmutableList<Node<T>>> Expand(T value)
        {
            value.AssertNotNull(nameof(value));

            if (!Contains(value))
            {
                throw new ArgumentException($"Item '{value}' not in graph");
            }

            var modifiableNode = GetNode(value);

            if (_nodesState[modifiableNode].IsUnexplored)
            {
                await InternalExpand(modifiableNode);
            }

            return modifiableNode.CurrentNeighbors;
        }

        private async Task InternalExpand(Node<T> modifiableNode)
        {
            ImmutableList<(T value, double connectionWeight)> expandedChildren = await _expandFunction(modifiableNode.Value);
            modifiableNode.ClearConnections();

            foreach ((var childValue, double connectionWeight) in expandedChildren)
            {
                Node<T> addedNode = TryAdd(childValue)
                                    // if node already exists:
                                    ?? GetNode(childValue);

                modifiableNode.AddConnection(addedNode, connectionWeight);
            }

            _nodesState[modifiableNode] = _nodesState[this[modifiableNode.Value]].WithUpdate(false);
        }

        /// <inheritdoc />
        public override bool Equals(object? other) => other is IGraph<T> graph && graph.Id == Id;

        /// <inheritdoc />
        public override int GetHashCode() => base.GetHashCode();
    }
}