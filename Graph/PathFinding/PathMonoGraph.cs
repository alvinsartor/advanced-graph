﻿using System.Collections.Generic;
using System.Collections.Immutable;
using Graph.Extensions;

namespace Graph.PathFinding
{
    /// <summary>
    /// A path in which all nodes belong to the same graph.
    /// </summary>
    /// <seealso cref="Path{T}" />
    public sealed class PathMonoGraph<T> : Path<T> where T : notnull
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Path{T}"/> class.
        /// </summary>
        /// <param name="edges">The edges.</param>
        internal PathMonoGraph(ImmutableList<Edge<T>> edges) => Edges = edges;

        /// <summary>
        /// Initializes a new instance of the <see cref="Path{T}"/> class.
        /// </summary>
        /// <param name="nodes">The explored nodes. The last node of the list is considered the destination and the first is the start.</param>
        internal PathMonoGraph( LinkedList<Node<T>> nodes)
        {
            var result = new List<Edge<T>>();
            var destination = nodes.PopLast();

            while (nodes.Count > 0)
            {
                var actual = nodes.PopLast();
                if (actual.IsConnectedTo(destination))
                {
                    result.Add(actual.Edges[destination.Value]);
                    destination = actual;
                }
            }

            Edges = result.ToImmutableList().Reverse();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Path{T}"/> class.
        /// </summary>
        /// <param name="parents">A dictionary containing the parents (values) of the nodes (keys).</param>
        /// <param name="destination">The destination node.</param>
        internal PathMonoGraph(IReadOnlyDictionary<Node<T>, Node<T>?> parents,  Node<T> destination) 
        {
            var result = new List<Edge<T>>();
            var actual = destination;
            var previous = parents[actual];

            while (previous != null)
            {
                result.Add(previous.Edges[actual.Value]);

                actual = previous;
                previous = parents[actual];
            }

            Edges = result.ToImmutableList().Reverse();
        }
    }
}