﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Graph.Extensions;
using Graph.Interfaces;

namespace Graph.PathFinding
{
    /// <summary>
    /// Extensions used to retrieve a path across multiple graphs.
    /// </summary>
    public static class PathFindingMultiGraphExtensions
    {
        /// <summary>
        /// Performs a A* search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="multiGraph">The collection of graphs.</param>
        /// <param name="initial">The initial value.</param>
        /// <param name="final">The final value.</param>
        /// <param name="getHeuristic">The heuristic function,
        /// represents the approximation of the distance between the current node and the destination.</param>
        public static async Task<Path<T>> AStarSearch<T>(
            this ImmutableList<IGraph<T>> multiGraph,
            T initial,
            T final,
            Func<T, double> getHeuristic) where T : notnull
        {
            ThrowIfNodeDoesNotBelongToGraph(multiGraph, initial);

            var pathCosts = new Dictionary<Node<T>, double>();
            var pathHeuristics = new Dictionary<Node<T>, double>();
            var parents = new Dictionary<Node<T>, Node<T>?>();

            var open = new LinkedList<Node<T>>();
            var closed = new HashSet<Node<T>>();

            var initialNode = multiGraph.First(graph => graph.Contains(initial))[initial];

            open.AddFirst(initialNode);
            pathCosts[initialNode] = 0;
            pathHeuristics[initialNode] = getHeuristic(initial);
            parents[initialNode] = null;

            while (open.Count > 0)
            {
                var actual = open.PopFirst();
                closed.Add(actual);

                if (actual.Value.Equals(final))
                {
                    return new PathMultiGraph<T>(multiGraph, parents, actual);
                }

                var neighbors = await GetNeighborsInMultiGraph(multiGraph, actual);

                foreach (var edge in neighbors)
                {
                    var gq = pathCosts[actual] + edge.Weight;

                    if (!pathCosts.ContainsKey(edge.DestinationNode) || gq < pathCosts[edge.DestinationNode])
                    {
                        parents[edge.DestinationNode] = actual;
                        pathCosts[edge.DestinationNode] = gq;
                        pathHeuristics[edge.DestinationNode] = gq + getHeuristic(edge.DestinationNode.Value);
                        open.AddOrdered(edge.DestinationNode, e => pathHeuristics[e]);
                        if (closed.Contains(edge.DestinationNode))
                        {
                            closed.Remove(edge.DestinationNode);
                        }
                    }
                }
            }

            return PathMultiGraph<T>.Empty;
        }

        private static async Task<IEnumerable<Edge<T>>> GetNeighborsInMultiGraph<T>(
            ImmutableList<IGraph<T>> multiGraph,
            Node<T> actual) where T : notnull
        {
            var neighbors = new Dictionary<T, Edge<T>>();
            foreach (var graph in multiGraph.Where(graph => graph.Contains(actual.Value)))
            {
                var graphNeighbors = await graph[actual.Value].GetAllNeighbors();
                foreach (var neighbor in graphNeighbors.Where(neighbor => !neighbors.ContainsKey(neighbor.Value)))
                {
                    neighbors.Add(neighbor.Value, graph[actual.Value].Edges[neighbor.Value]);
                }
            }

            return neighbors.Values;
        }

        private static void ThrowIfNodeDoesNotBelongToGraph<T>(ImmutableList<IGraph<T>> multiGraph, T value)
            where T : notnull
        {
            if (!multiGraph.Any(graph => graph.Contains(value)))
            {
                throw new ArgumentException($"The item {value} does not belong to the multiGraph.");
            }
        }
    }
}