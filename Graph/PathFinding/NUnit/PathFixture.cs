﻿using NUnit.Framework;

namespace Graph.PathFinding.NUnit
{
    [TestFixture]
    internal abstract class PathFixture
    {
        
        protected abstract Path<int> CreatePath(int initialNodeValue,  params (int node, double weight)[] nodes);

        [Test]
        public void EmptyPathDoesNotContainEdges()
        {
            var path = Path<int>.Empty;

            Assert.That(path.Cost, Is.EqualTo(0));
            Assert.That(path.Edges.Count, Is.EqualTo(0));
            Assert.That(path.Nodes.Count, Is.EqualTo(0));
        }

        [Test]
        public void PathIsAlwaysInitializedWithNewGuid()
        {
            Assert.That(CreatePath(0, (1, 1d)).Id, Is.Not.EqualTo(CreatePath(0, (1, 1d)).Id));
        }

        [Test]
        public void PathContainsExpectedValues()
        {
            var path = CreatePath(42, (38, 10d), (12, 7d));

            Assert.That(path.Cost, Is.EqualTo(17));
            Assert.That(path.Edges.Count, Is.EqualTo(2));
            Assert.That(path.Nodes.Count, Is.EqualTo(3));

            Assert.That(path.Nodes[0].Value, Is.EqualTo(42));
            Assert.That(path.Nodes[1].Value, Is.EqualTo(38));
            Assert.That(path.Nodes[2].Value, Is.EqualTo(12));
        }     

        [Test]
        public void CallingToStringOnAPathReturnsThePathRepresentation()
        {
            var path = CreatePath(42, (38, 10d), (12, 7d));
            Assert.That(path.ToString(i => i.ToString()), Is.EqualTo("42 -> 38 -> 12"));
        }
    }
}