﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Graph.Interfaces;
using NUnit.Framework;

namespace Graph.PathFinding.NUnit
{
    [TestFixture]
    internal sealed class PathFindingMultiGraphExtensionsFixture
    {        
        
        private static ImmutableList<IGraph<int>> GetMultiGraph()
        {
            var graphLayer0 = new Graph<int>("layer0");
            var graphLayer1 = new Graph<int>("layer1");
            var graphLayer2 = new Graph<int>("layer2");

            var node00 = graphLayer0.Add(0);
            var node01 = graphLayer0.Add(1);
            var node02 = graphLayer0.Add(2);

            node00.AddConnection(node01);
            node00.AddConnection(node02);

            var node11 = graphLayer1.Add(1);
            var node12 = graphLayer1.Add(2);
            var node13 = graphLayer1.Add(3);
            var node14 = graphLayer1.Add(4);
            var node15 = graphLayer1.Add(5);
            var node16 = graphLayer1.Add(6);

            node11.AddConnection(node13);
            node11.AddConnection(node14);
            node12.AddConnection(node15);
            node12.AddConnection(node16);

            var node23 = graphLayer2.Add(3);
            var node24 = graphLayer2.Add(4);
            var node27 = graphLayer2.Add(7);
            var node28 = graphLayer2.Add(8);
            var node29 = graphLayer2.Add(9);
            var node210 = graphLayer2.Add(10);

            node23.AddConnection(node27);
            node23.AddConnection(node28);
            node24.AddConnection(node29);
            node24.AddConnection(node210);

            var node011 = graphLayer0.Add(11);

            node210.AddConnection(node011);

            return new[] {graphLayer0, graphLayer1, graphLayer2}.Cast<IGraph<int>>().ToImmutableList();
        }
       
        // A Star

        [Test]
        public void AStarThrowsIfNodeIsNotInGraph()
        {
            var multiGraph = GetMultiGraph();

            var exception = Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var _ = await multiGraph.AStarSearch(15, 19, n => 1);
            });
            Assert.That(exception.Message.Contains("does not belong"));            
        }

        [Test]
        public async Task AStarReturnsAnEmptyPathIfNoPathHasBeenFound()
        {
            var path = await GetMultiGraph().AStarSearch(0, 21, n => 1);
            Assert.That(path.IsEmpty);
        }

        [Test]
        public async Task AStarGetsTheRightPath()
        {
            var multiGraph = GetMultiGraph();

            var path = await multiGraph.AStarSearch(0, 11, (n) => Math.Abs(11 - n));

            Assert.That(path.ToString(x => x.ToString(), showGraphName: true), 
                Is.EqualTo("0 (layer0) -> 1 (layer0) -> 4 (layer1) -> 10 (layer2) -> 11 (layer0)"));
        }

        [Test]
        public async Task WhenMultipleEdgesLeadToTheSameDestinationTheCheapestIsChosen()
        {
            var graphLayer0 = new Graph<int>("layer0");
            var graphLayer1 = new Graph<int>("layer1");

            var node00 = graphLayer0.Add(0);
            var node01 = graphLayer0.Add(1);
            var node02 = graphLayer0.Add(2);
            var node03 = graphLayer0.Add(3);

            var node11 = graphLayer1.Add(1);
            var node12 = graphLayer1.Add(2);

            node00.AddConnection(node01, 10);
            node01.AddConnection(node02, 100);
            node02.AddConnection(node03, 10);

            node11.AddConnection(node12, 10);

            var multiGraph = new[] {graphLayer0, graphLayer1}.Cast<IGraph<int>>().ToImmutableList();

            var path = await multiGraph.AStarSearch(0, 3, (n) => 7);

            Assert.That(path.ToString(x => x.ToString(), showGraphName: true), 
                Is.EqualTo("0 (layer0) -> 1 (layer0) -> 2 (layer1) -> 3 (layer0)"));
            Assert.That(path.Cost, Is.EqualTo(30));
        }
    }
}