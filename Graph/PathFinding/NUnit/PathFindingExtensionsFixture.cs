﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Graph.PathFinding.NUnit
{
    [TestFixture]
    internal sealed class PathFindingExtensionsFixture
    {
        private static readonly Dictionary<char, double> Heuristic = new Dictionary<char, double>
        {
            {'A', 40d},
            {'B', 30d},
            {'C', 25d},
            {'D', 20d},
            {'E', 9d},
            {'F', 10d},
            {'G', 18d},
            {'H', 0d}
        };

        private static double GetHeuristic(char value)
        {
            return Heuristic[value];
        }

        
        private static Graph<char> GetGraph()
        {
            var graph = new Graph<char>();

            var nodeA = graph.Add('A');
            var nodeB = graph.Add('B');
            var nodeC = graph.Add('C');
            var nodeD = graph.Add('D');
            var nodeE = graph.Add('E');
            var nodeF = graph.Add('F');
            var nodeG = graph.Add('G');
            var nodeH = graph.Add('H');

            nodeA.AddConnection(nodeB, 5d);
            nodeA.AddConnection(nodeC, 8d);

            nodeB.AddConnection(nodeD, 6d);
            nodeB.AddConnection(nodeF, 10d);

            nodeC.AddConnection(nodeD, 2d);
            nodeC.AddConnection(nodeG, 7d);

            nodeD.AddConnection(nodeE, 4d);

            nodeE.AddConnection(nodeF, 2d);
            nodeE.AddConnection(nodeH, 5d);
            nodeE.AddConnection(nodeG, 3d);

            nodeF.AddConnection(nodeH, 9d);

            nodeG.AddConnection(nodeH, 6d);

            return graph;
        }


        // DEPTH FIRST

        [Test]
        public void DepthFirstThrowsIfNodeIsNotInGraph()
        {
            var graph = new Graph<int>();
            var node1 = new Node<int>(graph, 1);
            var node2 = new Node<int>(graph, 2);

            var exception = Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var _ = await graph.FirstDepthSearch(node1, node2);
            });
            Assert.That(exception.Message.Contains("does not belong"));

            var node3 = graph.Add(3);
            node3.AddConnection(node2);
            Assert.DoesNotThrowAsync(async () =>
            {
                var _ = await graph.FirstDepthSearch(node3, node2);
            });
        }

        [Test]
        public async Task DepthFirstReturnsAnEmptyPathIfNoPathHasBeenFound()
        {
            var graph = new Graph<int>();
            var node1 = graph.Add(1);
            var node2 = graph.Add(2);

            var path = await graph.FirstDepthSearch(node1, node2);            
            Assert.That(path.IsEmpty);
        }

        [Test]
        public async Task DepthFirstGetsTheRightPath()
        {
            var graph = GetGraph();
            var initial = graph.GetNode('A');
            var final = graph.GetNode('H');

            var path = await graph.FirstDepthSearch(initial, final);
            for (var i = 0; i < path.Nodes.Count - 1; i++)
            {
                var current = path.Nodes[i];
                var next = path.Nodes[i + 1];

                Assert.That(current.IsConnectedTo(next));
            }

            Assert.That(path.Nodes.First().Value == 'A');
            Assert.That(path.Nodes.Last().Value == 'H');
        }

        // A Star

        [Test]
        public void AStarThrowsIfNodeIsNotInGraph()
        {
            var graph = new Graph<int>();
            var node1 = new Node<int>(graph, 1);
            var node2 = new Node<int>(graph, 2);

            var exception = Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var _ = await graph.AStarSearch(node1, node2, n => 0);
            });
            Assert.That(exception.Message.Contains("does not belong"));

            var node3 = graph.Add(3);
            node3.AddConnection(node2);
            Assert.DoesNotThrowAsync(async () =>
            {
                var _ = await graph.AStarSearch(node3, node2, n => 1);
            });
        }

        [Test]
        public async Task AStarReturnsAnEmptyPathIfNoPathHasBeenFound()
        {
            var graph = new Graph<int>();
            var node1 = graph.Add(1);
            var node2 = graph.Add(2);

            var path = await graph.AStarSearch(node1, node2, node => 1);
            Assert.That(path.IsEmpty);
        }

        [Test]
        public async Task AStarGetsTheRightPath()
        {
            var graph = GetGraph();
            var initial = graph.GetNode('A');
            var final = graph.GetNode('H');

            var path = await graph.AStarSearch(initial, final, GetHeuristic);

            Assert.That(path.ToString(x => x.ToString()), Is.EqualTo("A -> C -> D -> E -> H"));
            Assert.That(path.Cost, Is.EqualTo(19));
        }
        
        [Test]
        public async Task AStarGetsAvoidUnwalkableNodes()
        {
            var graph = GetGraph();
            var initial = graph.GetNode('A');
            var final = graph.GetNode('H');
            
            HashSet<char> unwalkableNodes = new[] { 'C', 'D', 'G' }.ToHashSet();
            var path = await graph.AStarSearch(initial, final, GetHeuristic, unwalkableNodes);

            Assert.That(path.ToString(x => x.ToString()), Is.EqualTo("A -> B -> F -> H"));
            Assert.That(path.Cost, Is.EqualTo(24));
        }

        // BEST FIRST

        [Test]
        public void BestFirstThrowsIfNodeIsNotInGraph()
        {
            var graph = new Graph<int>();
            var node1 = new Node<int>(graph, 1);
            var node2 = new Node<int>(graph, 2);

            var exception = Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var _ = await graph.BestFirstSearch(node1, node2);
            });
            Assert.That(exception.Message.Contains("does not belong"));

            var node3 = graph.Add(3);
            node3.AddConnection(node2);
            Assert.DoesNotThrowAsync(async () =>
            {
                var _ = await graph.BestFirstSearch(node3, node2);
            });
        }

        [Test]
        public async Task BestFirstReturnsAnEmptyPathIfNoPathHasBeenFound()
        {
            var graph = new Graph<int>();
            var node1 = graph.Add(1);
            var node2 = graph.Add(2);

            var path = await graph.BestFirstSearch(node1, node2);
            Assert.That(path.IsEmpty);
        }

        [Test]
        public async Task BestFirstGetsTheRightPath()
        {
            var graph = GetGraph();
            var initial = graph.GetNode('A');
            var final = graph.GetNode('H');

            var path = await graph.BestFirstSearch(initial, final);

            Assert.That(path.ToString(x => x.ToString()), Is.EqualTo("A -> C -> D -> E -> H"));
            Assert.That(path.Cost, Is.EqualTo(19));
        }
    }
}