﻿using System.Collections.Generic;
using System.Collections.Immutable;
using NUnit.Framework;

namespace Graph.PathFinding.NUnit
{
    [TestFixture]
    internal sealed class PathMonoGraphFixture : PathFixture
    {
        /// <inheritdoc />
        protected override Path<int> CreatePath(int initialNodeValue, params (int node, double weight)[] nodes)
        {
            var graph = new Graph<int>();
            var previous = graph.Add(initialNodeValue);
            
            var edges = new List<Edge<int>>();
            foreach (var (nodeValue, weight) in nodes)
            {
                var node = graph.Add(nodeValue);
                previous.AddConnection(node, weight);

                edges.Add(previous.Edges[nodeValue]);
                previous = node;
            }

            return new PathMonoGraph<int>(edges.ToImmutableList());
        }

        /// <summary>
        /// Function to check multiple constructors.
        /// It expects the path: 42 ->(10) 38 ->(7) 12
        /// </summary>
        /// <param name="path">The path.</param>
        private void PathContainsExpectedValues( Path<int> path)
        {            
            Assert.That(path.Cost, Is.EqualTo(17));
            Assert.That(path.Edges.Count, Is.EqualTo(2));
            Assert.That(path.Nodes.Count, Is.EqualTo(3));

            Assert.That(path.Nodes[0].Value, Is.EqualTo(42));
            Assert.That(path.Nodes[1].Value, Is.EqualTo(38));
            Assert.That(path.Nodes[2].Value, Is.EqualTo(12));
        }

        [Test]
        public void PathInitializedWithLinkedListContainsCorrectValues()
        {
            var graph = new Graph<int>();
            var node42 = graph.Add(42);
            var node38 = graph.Add(38);
            var node12 = graph.Add(12);

            node42.AddConnection(node38, 10);
            node38.AddConnection(node12, 7);

            Path<int> path = new PathMonoGraph<int>(new LinkedList<Node<int>>(new[]
            {
                node42,
                node38,
                node12
            }));

            PathContainsExpectedValues(path);            
        }

        [Test]
        public void PathInitializedWithDictionaryContainsCorrectValues()
        {
            var graph = new Graph<int>();
            var node42 = graph.Add(42);
            var node38 = graph.Add(38);
            var node12 = graph.Add(12);
            var node99 = graph.Add(99);

            node42.AddConnection(node38, 10);
            node38.AddConnection(node12, 7);

            var path = new PathMonoGraph<int>(new Dictionary<Node<int>, Node<int>?>
            {
                {node42, null },
                {node38, node42 },
                {node12, node38 },
                {node99, node99 }
            }, node12);

            PathContainsExpectedValues(path);
        }
    }
}
