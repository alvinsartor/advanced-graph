﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Graph.Extensions;
using Graph.Interfaces;

namespace Graph.PathFinding
{
    /// <summary>
    /// A path that crosses multiple graphs.
    /// </summary>
    /// <seealso cref="Path{T}" />
    public sealed class PathMultiGraph<T> : Path<T> where T : notnull
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PathMultiGraph{T}"/> class.
        /// </summary>
        /// <remarks>Use this constructor to build a path in a multi-graph.</remarks>
        /// <param name="multiGraph">The multi-graph.</param>
        /// <param name="parents">A dictionary containing the parents of the nodes.</param>
        /// <param name="destination">The destination node.</param>
        internal PathMultiGraph(
            IReadOnlyList<IGraph<T>> multiGraph,
            IReadOnlyDictionary<Node<T>, Node<T>?> parents,
            Node<T> destination) 
        {
            var result = new List<Edge<T>>();
            var actual = destination;
            var previous = parents[actual];

            while (previous != null)
            {
                var edge = GetEdgeFromMultiGraph(multiGraph, previous.Value, actual.Value);
                result.Add(edge);

                actual = previous;
                previous = parents[actual];
            }

            Edges = result.ToImmutableList().Reverse();
        }

        /// <summary>
        /// Gets the edge with the lowest weight that connects the specified values from the multiGraph.
        /// </summary>
        /// <param name="multiGraph">The multiGraph.</param>
        /// <param name="startValue">The start value.</param>
        /// <param name="destinationValue">The destination value.</param>
        private static Edge<T> GetEdgeFromMultiGraph(IEnumerable<IGraph<T>> multiGraph, T startValue, T destinationValue)
        {
            var edges = multiGraph
                .Where(graph => graph.Contains(startValue) && graph[startValue].IsConnectedTo(destinationValue))
                .Select(graph => graph[startValue].Edges[destinationValue])
                .ToList();

            if (edges.Count == 0)
            {
                throw new ArgumentException("No edge found between start and destination in the multiGraph.");
            }

            return edges.MinBy(edge => edge.Weight);
        }
    }
}