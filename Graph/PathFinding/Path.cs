﻿using System;
using System.Collections.Immutable;
using System.Linq;

namespace Graph.PathFinding
{
    /// <summary>
    /// An ordered collection of nodes, representing a path from A to B.
    /// </summary>
    public class Path<T> where T : notnull
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="Path{T}"/> class from being created.
        /// </summary>
        protected Path()
        {
            Id = Guid.NewGuid();
            Edges = ImmutableList<Edge<T>>.Empty;
        }

        /// <summary>
        /// Gets the path identifier.
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Gets an empty path.
        /// </summary>

        public static Path<T> Empty => new Path<T>();

        /// <summary>
        /// Gets a value indicating whether this path is empty.
        /// </summary>
        public bool IsEmpty => Edges.Count == 0;

        /// <summary>
        /// Gets the edges of the path.
        /// </summary>
        public ImmutableList<Edge<T>> Edges { get; protected set; }

        /// <summary>
        /// Gets the nodes of the path.
        /// </summary>
        public ImmutableList<Node<T>> Nodes => Edges.Count > 0
            ? Edges.Select(x => x.StartNode).ToImmutableList().Add(Edges.Last().DestinationNode)
            : ImmutableList<Node<T>>.Empty;

        /// <summary>
        /// Gets the total cost of the path.
        /// </summary>
        public double Cost => Edges.Count > 0
            ? Edges.Select(x => x.Weight).Sum()
            : 0;

        /// <inheritdoc />
        public override string ToString()
        {
            if (IsEmpty)
            {
                return "The path is empty.";
            }

            return Edges.Aggregate(
                $"{Edges[0].StartNode}", 
                (current, edge) => current + $" -> {edge.DestinationNode}");
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <param name="toString">Function to obtain the string representation from the value of the node.</param>
        /// <param name="showWeights">>Determines whether the connection weights are printed.</param>
        /// <param name="showGraphName">Determines whether the graph name is printed for each node (useful when using multi-graphs).</param>
        public string ToString(Func<T, string> toString, bool showWeights = false, bool showGraphName = false)
        {
            string GraphName(Node<T> node) => showGraphName ? $" ({node.Graph.Name})" : "";
            string EdgeWeight(Edge<T> edge) => showWeights ? $"({edge.Weight})" : "";
            string PrintNode(Node<T> node) => $"{node.ToString(toString)}{GraphName(node)}";

            if (IsEmpty)
            {
                return "The path is empty.";
            }

            return Edges.Aggregate(
                PrintNode(Edges[0].StartNode), 
                (current, edge) => current + $" ->{EdgeWeight(edge)} {PrintNode(edge.DestinationNode)}");
        }
    }
}