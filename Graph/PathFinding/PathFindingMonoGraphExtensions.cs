﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Graph.Extensions;
using Graph.Interfaces;

namespace Graph.PathFinding
{
    /// <summary>
    /// Extensions used to retrieve a path on a graphs.
    /// </summary>
    public static class PathFindingMonoGraphExtensions
    {
        /// <summary>
        /// Performs a A* search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial node.</param>
        /// <param name="final">The final node.</param>
        /// <param name="getHeuristic">The heuristic function,
        /// represents the approximation of the distance between the current node and the destination.</param>
        [Obsolete("This implementation has higher computational complexity. " +
                  "If tests show that the newer implementation has the same results, this will be retired.")]
        public static async Task<Path<T>> AStarSearchOld<T>(
            this IGraph<T> graph,
            Node<T> initial,
            Node<T> final,
            Func<Node<T>, double> getHeuristic) where T : notnull
        {
            ThrowIfNodeDoesNotBelongToGraph(graph, initial);

            var pathCosts = new Dictionary<Node<T>, double>();
            var pathHeuristics = new Dictionary<Node<T>, double>();
            var parents = new Dictionary<Node<T>, Node<T>?>();

            var open = new LinkedList<Node<T>>();
            var closed = new HashSet<Node<T>>();

            open.AddFirst(initial);
            pathCosts[initial] = 0;
            parents[initial] = null;

            while (open.Count > 0)
            {
                var actual = open.PopFirst();
                closed.Add(actual);

                if (actual.Equals(final))
                {
                    return new PathMonoGraph<T>(parents, final);
                }

                var successors = (await actual.GetAllNeighbors()).ToImmutableHashSet();
                while (successors.Count > 0)
                {
                    var q = successors.First();
                    successors = successors.Remove(q);

                    var gq = pathCosts[actual] + actual.Edges[q.Value].Weight;

                    var openContainsQ = open.Contains(q);
                    var closedContainsQ = closed.Contains(q);

                    if (!openContainsQ && !closedContainsQ)
                    {
                        parents[q] = actual;
                        pathHeuristics[q] = gq + getHeuristic(q);
                        pathCosts[q] = gq;
                        open.AddOrdered(q, e => pathHeuristics[e]);
                    }
                    else if (pathCosts.ContainsKey(q) && gq < pathCosts[q])
                    {
                        parents[q] = actual;
                        pathCosts[q] = gq;
                        pathHeuristics[q] = gq + getHeuristic(q);
                        if (closedContainsQ)
                        {
                            closed.Remove(q);
                            open.AddOrdered(q, e => pathHeuristics[e]);
                        }
                    }
                }
            }

            return Path<T>.Empty;
        }

        /// <summary>
        /// Performs a A* search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial node.</param>
        /// <param name="final">The final node.</param>
        /// <param name="getHeuristic">The heuristic function,
        /// represents the approximation of the distance between the current node and the destination.</param>
        /// <param name="unwalkableNodes">Nodes that should be avoided during the search of a path.</param>
        public static async Task<Path<T>> AStarSearch<T>(
            this IGraph<T> graph,
            Node<T> initial,
            Node<T> final,
            Func<T, double> getHeuristic,
            HashSet<T>? unwalkableNodes = null) where T : notnull => 
            await graph.AStarSearch(initial.Value, final.Value, getHeuristic, unwalkableNodes);

        /// <summary>
        /// Performs a A* search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial value.</param>
        /// <param name="final">The final value.</param>
        /// <param name="getHeuristic">The heuristic function,
        /// represents the approximation of the distance between the current node and the destination.</param>
        /// <param name="unwalkableNodes">Nodes that should be avoided during the search of a path.</param>
        public static async Task<Path<T>> AStarSearch<T>(
            this IGraph<T> graph,
            T initial,
            T final,
            Func<T, double> getHeuristic,
            HashSet<T>? unwalkableNodes = null) where T : notnull => 
            await graph.AStarSearch(initial, (val) => final.Equals(val), getHeuristic, unwalkableNodes);

        /// <summary>
        /// Performs a A* search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial value.</param>
        /// <param name="hasArrived">A function determining whether the current node is the destination.</param>
        /// <param name="getHeuristic">The heuristic function,
        /// represents the approximation of the distance between the current node and the destination.</param>
        /// <param name="unwalkableNodes">Nodes that should be avoided during the search of a path.</param>
        public static async Task<Path<T>> AStarSearch<T>(
            this IGraph<T> graph,
            T initial,
            Func<T, bool> hasArrived,
            Func<T, double> getHeuristic,
            HashSet<T>? unwalkableNodes = null) where T : notnull
        {
            ThrowIfNodeDoesNotBelongToGraph(graph, initial);

            var pathCosts = new Dictionary<Node<T>, double>();
            var pathHeuristics = new Dictionary<Node<T>, double>();
            var parents = new Dictionary<Node<T>, Node<T>?>();

            var open = new LinkedList<Node<T>>();
            var closed = new HashSet<Node<T>>();
            var initialNode = graph[initial];

            open.AddFirst(initialNode);
            pathCosts[initialNode] = 0;
            pathHeuristics[initialNode] = getHeuristic(initial);
            parents[initialNode] = null;

            while (open.Count > 0)
            {
                var actual = open.PopFirst();
                closed.Add(actual);

                if (hasArrived(actual.Value))
                {
                    return new PathMonoGraph<T>(parents, actual);
                }

                var neighbors = await actual.GetAllNeighbors();
                var walkableNeighbors = unwalkableNodes == null ? neighbors : neighbors.Where(n => !unwalkableNodes.Contains(n.Value));
                foreach (var q in walkableNeighbors)
                {
                    var gq = pathCosts[actual] + actual.Edges[q.Value].Weight;

                    if (!pathCosts.ContainsKey(q) || gq < pathCosts[q])
                    {
                        parents[q] = actual;
                        pathCosts[q] = gq;
                        pathHeuristics[q] = gq + getHeuristic(q.Value);
                        open.AddOrdered(q, e => pathHeuristics[e]);
                        if (closed.Contains(q))
                        {
                            closed.Remove(q);
                        }
                    }
                }
            }

            return Path<T>.Empty;
        }

        /// <summary>
        /// Performs a best-first search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial node.</param>
        /// <param name="final">The final node.</param>
        public static async Task<Path<T>> BestFirstSearch<T>(
            this IGraph<T> graph,
            Node<T> initial,
            Node<T> final) where T : notnull
        {
            return await graph.BestFirstSearch(initial.Value, (val) => final.Value.Equals(val));
        }

        /// <summary>
        /// Performs a best-first search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial value.</param>
        /// <param name="hasArrived">A function determining whether the current node is the destination.</param>
        public static async Task<Path<T>> BestFirstSearch<T>(
            this IGraph<T> graph,
            T initial,
            Func<T, bool> hasArrived) where T : notnull
        {
            ThrowIfNodeDoesNotBelongToGraph(graph, initial);

            var pathCosts = new Dictionary<Node<T>, double>();
            var parents = new Dictionary<Node<T>, Node<T>?>();

            var open = new LinkedList<Node<T>>();
            var closed = new HashSet<Node<T>>();
            var initialNode = graph[initial];

            open.AddFirst(initialNode);
            pathCosts[initialNode] = 0;
            parents[initialNode] = null;

            while (open.Count > 0)
            {
                var actual = open.PopFirst();
                closed.Add(actual);

                if (hasArrived(actual.Value))
                {
                    return new PathMonoGraph<T>(parents, actual);
                }

                var neighbors = await actual.GetAllNeighbors();
                foreach (var q in neighbors)
                {
                    var gq = pathCosts[actual] + actual.Edges[q.Value].Weight;

                    if (!pathCosts.ContainsKey(q) || gq < pathCosts[q])
                    {
                        parents[q] = actual;
                        pathCosts[q] = gq;
                        open.AddOrdered(q, e => pathCosts[e]);
                        if (closed.Contains(q))
                        {
                            closed.Remove(q);
                        }
                    }
                }
            }

            return Path<T>.Empty;
        }

        /// <summary>
        /// Performs a first-depth search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial node.</param>
        /// <param name="final">The final node.</param>
        public static async Task<Path<T>> FirstDepthSearch<T>(
            this IGraph<T> graph,
            Node<T> initial,
            Node<T> final) where T : notnull => await graph.FirstDepthSearch(initial.Value, (val) => final.Value.Equals(val));


        /// <summary>
        /// Performs a first-depth search to find the path from the specified nodes.
        /// </summary>
        /// <typeparam name="T">The nodes type.</typeparam>
        /// <param name="graph">The graph.</param>
        /// <param name="initial">The initial value.</param>
        /// <param name="hasArrived">A function determining whether the current node is the destination.</param>
        public static async Task<Path<T>> FirstDepthSearch<T>(
            this IGraph<T> graph,
            T initial,
            Func<T, bool> hasArrived) where T : notnull
        {
            ThrowIfNodeDoesNotBelongToGraph(graph, initial);

            var open = new LinkedList<Node<T>>();
            var closed = new LinkedList<Node<T>>();
            var initialNode = graph[initial];
            open.AddFirst(initialNode);

            while (open.Count > 0)
            {
                var actual = open.PopFirst();
                closed.AddLast(actual);

                if (hasArrived(actual.Value))
                {
                    return new PathMonoGraph<T>(closed);
                }

                var neighbors = await actual.GetAllNeighbors();
                var successors = neighbors.Except(open.Union(closed));
                open.PushFirst(successors);
            }

            return Path<T>.Empty;
        }

        private static void ThrowIfNodeDoesNotBelongToGraph<T>(IGraph<T> graph, Node<T> node) where T : notnull
        {
            if (!graph.Contains(node.Id))
            {
                throw new ArgumentException($"The node {node} does not belong to the graph {graph}.");
            }
        }

        private static void ThrowIfNodeDoesNotBelongToGraph<T>(IGraph<T> graph, T value) where T : notnull
        {
            if (!graph.Contains(value))
            {
                throw new ArgumentException($"The item {value} does not belong to the graph {graph}.");
            }
        }
    }
}