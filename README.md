# Advanced Graph

AdvancedGraph (from now on, AG) is a library that offers a graph out of the box.   

It allows the following: 

* Store your data in a graph data structure   
* Look for cycles   
* Multiple graph types   
    * Single graph
    * Multigraphs
    * Dynamic graphs        
* Pathfinding algorithms
    * A*
    * Best-First
    * Depth-First
 

---

# Usage

### Graph content

Graphs are **generic**, so you can use any kind of data as nodes.  
In the following examples, we'll use this simple `struct Person`:

```
public struct Person
{
    private readonly string _name;
    private readonly int _age;
    private readonly string _nationality;
    public Person(string name, int age, string nationality) => (_name, _age, _nationality) = (name, age, nationality);
    public override string ToString() => $"{_name}, age:{_age} ({_nationality})";
    public override bool Equals(object? obj) => obj is Person p && p.ToString() == ToString();
    public override int GetHashCode() => HashCode.Combine(_name, _age, _nationality);
}
```

### Graph Initialization and Filling

Let's define a graph that represents friendship relations between persons:

```
Graph<Person> friendshipGraph = new();
```

Next: add some nodes   

```
// graph is populated directly...
Node<Person> lucyNode = friendshipGraph.Add(new Person("Lucy", 25, "IT"));
Node<Person> markNode = friendshipGraph.Add(new Person("Mark", 30, "ES"));

// ...or through nodes creation
Node<Person> maryNode = new Node<Person>(friendshipGraph, new Person("Mary", 35, "US"));
Node<Person> johnNode = new Node<Person>(friendshipGraph, new Person("John", 51, "UK"));
```

When we have the nodes, we can add the edges

```
// edges are created using the nodes
lucyNode.AddBidirectionalConnection(markNode); // bidirectional
markNode.AddConnection(maryNode); // single-direction connection 
maryNode.AddConnection(johnNode, 5); // edges can have a weight
```

### Pathfinding

Finding the path from A to B is really easy:

```
Path<Person> lucyToJohn = await friendshipGraph.FirstDepthSearch(lucyNode, johnNode);
string pathToString = lucyToJohn.ToString(person => person.ToString());
Console.WriteLine(pathToString);

// output: "Lucy, age:25 (IT) -> Mark, age:30 (ES) -> Mary, age:35 (US) -> John, age:51 (UK)"
```

### Multigraphs

Just by adding other graphs...

```
Graph<Person> careerGraph = new Graph<Person>();
Node<Person> lucyNodeCareer = careerGraph.Add(new Person("Lucy", 25, "IT"));
Node<Person> johnNodeCareer = careerGraph.Add(new Person("John", 51, "UK"));
lucyNodeCareer.AddConnection(johnNodeCareer);
```

And stacking them together in a ImmutableList...

```
ImmutableList<IGraph<Person>> multiGraph = new[] { friendshipGraph, careerGraph }.Cast<IGraph<Person>>().ToImmutableList();
```

You can find inter-graph paths

```
Path<Person> lucyToJohn = await multiGraph.AStarSearch(lucyNode.Value, johnNode.Value, person => 1d);
string pathToString = lucyToJohn.ToString(person => person.ToString());
Console.WriteLine(pathToString);

// The shortest path is found, in this case the one in careerGraph, even though the specified nodes belong to friendshipGraph
// output "Lucy, age:25 (IT) -> John, age:51 (UK)"
```